#include "widgetpersonnage.h"
#include "ui_widgetpersonnage.h"
#include "sortilege.h"
#include "sortileges.h"
#include "mainwindow.h"
#include "widgetajoutsorts.h"

WidgetPersonnage::WidgetPersonnage(QWidget *parent) :
    QWidget(parent),
    current_index(0),
    sort_index(0),
    ui(new Ui::WidgetPersonnage),
    Edition(false),
    modelSort(nullptr)
{
    ui->setupUi(this);
}

WidgetPersonnage::WidgetPersonnage(QWidget *parent, Personnage &p, int i) :
    QWidget(parent),
    current_index(i),
    sort_index(0),
    ui(new Ui::WidgetPersonnage),
    Edition(false)
{
    QRegExpValidator* validator = new QRegExpValidator(QRegExp("[^;]*"),this);


    ui->setupUi(this);
    ui->Nom_edit->setReadOnly(true);
    ui->Nom_edit->setText(p.get_nom());
    ui->Nom_edit->setValidator(validator);
    ui->Race_edit->setText(p.get_race());
    ui->Race_edit->setReadOnly(true);
    ui->Race_edit->setValidator(validator);
    ui->Niveau_edit->setValue(p.get_niveau());
    ui->Niveau_edit->setReadOnly(true);
    ui->Description_edit->setReadOnly(true);
    ui->Description_edit->setText(p.get_desc());
    ui->Force_edit->setReadOnly(true);
    ui->Force_edit->setValue(p.get_carac().get_force());
    ui->Dexterite_edit->setReadOnly(true);
    ui->Dexterite_edit->setValue(p.get_carac().get_dexterite());
    ui->Constitution_edit->setReadOnly(true);
    ui->Constitution_edit->setValue(p.get_carac().get_constitution());
    ui->Intelligence_edit->setReadOnly(true);
    ui->Intelligence_edit->setValue(p.get_carac().get_intelligence());
    ui->Sagesse_edit->setReadOnly(true);
    ui->Sagesse_edit->setValue(p.get_carac().get_sagesse());
    ui->Charisme_edit->setReadOnly(true);
    ui->Charisme_edit->setValue(p.get_carac().get_charisme());
    ui->ClasseEdit->setReadOnly(true);
    ui->ClasseEdit->setText(p.get_classes().get_classe().get_nom());
    ui->DominanteEdit->setText(p.get_classes().get_classe().get_dominante());
    ui->DominanteEdit->setReadOnly(true);
    ui->DominanteEdit->setValidator(validator);
    ui->BabEdit->setValue(p.get_classes().get_classe().get_bab());
    ui->BabEdit->setReadOnly(true);
    ui->Ajout_sort->setDisabled(true);
    ui->Supp_sort->setDisabled(true);


    Sortileges *sorts = new Sortileges(p.get_sorts(),ui->Sortileges_edit);
    ui->Sortileges_edit->setModel(sorts);
    modelSort = sorts;

    MainWindow * win = qobject_cast<MainWindow*>(QApplication::activeWindow());
    connect(win,&MainWindow::Personnage_deleted,this,&WidgetPersonnage::Personnage_was_deleted);
}

void WidgetPersonnage::ChangePersonnage(Personnage &p, int i)
{
    current_index = i;

    ui->Nom_edit->setReadOnly(true);
    ui->Nom_edit->setText(p.get_nom());
    ui->Race_edit->setText(p.get_race());
    ui->Race_edit->setReadOnly(true);
    ui->Niveau_edit->setValue(p.get_niveau());
    ui->Niveau_edit->setReadOnly(true);
    ui->Description_edit->setReadOnly(true);
    ui->Description_edit->setText(p.get_desc());
    ui->Force_edit->setReadOnly(true);
    ui->Force_edit->setValue(p.get_carac().get_force());
    ui->Dexterite_edit->setReadOnly(true);
    ui->Dexterite_edit->setValue(p.get_carac().get_dexterite());
    ui->Constitution_edit->setReadOnly(true);
    ui->Constitution_edit->setValue(p.get_carac().get_constitution());
    ui->Intelligence_edit->setReadOnly(true);
    ui->Intelligence_edit->setValue(p.get_carac().get_intelligence());
    ui->Sagesse_edit->setReadOnly(true);
    ui->Sagesse_edit->setValue(p.get_carac().get_sagesse());
    ui->Charisme_edit->setReadOnly(true);
    ui->Charisme_edit->setValue(p.get_carac().get_charisme());
    ui->ClasseEdit->setReadOnly(true);
    ui->ClasseEdit->setText(p.get_classes().get_classe().get_nom());
    ui->DominanteEdit->setText(p.get_classes().get_classe().get_dominante());
    ui->DominanteEdit->setReadOnly(true);
    ui->BabEdit->setValue(p.get_classes().get_classe().get_bab());
    ui->BabEdit->setReadOnly(true);
    ui->Ajout_sort->setDisabled(true);
    ui->Supp_sort->setDisabled(true);

    /*NiveauClasse nc = p.get_classes();
    QList<QString> *l = (static_cast<Sortileges>(p.get_sorts())).filter_list_by_class(nc);
    QStringList list = QStringList(*l);
    QStringListModel *model = new QStringListModel(list);*/
    Sortileges *sorts = new Sortileges(p.get_sorts(),ui->Sortileges_edit);
    ui->Sortileges_edit->setModel(sorts);
    modelSort = sorts;
    /*ui->Sortileges_edit->clear();
    for(int i=0; i<p.get_sorts().count();i++){
        QListWidgetItem *sort = new QListWidgetItem(p.get_sorts().at(i).get_nom());
        ui->Sortileges_edit->addItem(sort);
    }*/
}

WidgetPersonnage::~WidgetPersonnage()
{
    delete ui;
}

QModelIndex search_item(QString nom){
    MainWindow * win = qobject_cast<MainWindow*>(QApplication::activeWindow());
    Sortileges *sorts = static_cast<Sortileges*>(win->modelSort);
    int nb = sorts->rowCount();
    QModelIndex index = QModelIndex();
    for (int i = 0; i<nb; i++){
        index = sorts->index(i,0);
        QString s = qvariant_cast<QString>(sorts->data(index,Qt::DisplayRole));
        if(nom == s)
            return index;
    }
    return index;
}

void WidgetPersonnage::on_Sortileges_edit_doubleClicked(const QModelIndex &index)
{
    sort_index = index.row();
    QString selected_name = qvariant_cast<QString>(ui->Sortileges_edit->model()->data(index,Qt::DisplayRole));
    QModelIndex selected_index = search_item(selected_name);
    MainWindow * win = qobject_cast<MainWindow*>(QApplication::activeWindow());
    Sortileges *sorts = static_cast<Sortileges*>(win->modelSort);
    Sortilege selected = qvariant_cast<Sortilege>(sorts->data(selected_index,Qt::EditRole));
    //Sortilege selected = qvariant_cast<Sortilege>(ui->Sortileges_edit->model()->data(index,Qt::EditRole));
    emit(sortilege_choisi(&selected, selected_index.row()));
}

void WidgetPersonnage::on_pushButton_3_clicked()
{
    if(!Edition) {
        ui->Nom_edit->setReadOnly(false);
        ui->Race_edit->setReadOnly(false);
        ui->Niveau_edit->setReadOnly(false);
        ui->Description_edit->setReadOnly(false);
        ui->Force_edit->setReadOnly(false);
        ui->Dexterite_edit->setReadOnly(false);
        ui->Constitution_edit->setReadOnly(false);
        ui->Intelligence_edit->setReadOnly(false);
        ui->Sagesse_edit->setReadOnly(false);
        ui->Charisme_edit->setReadOnly(false);
        ui->ClasseEdit->setReadOnly(false);
        ui->BabEdit->setReadOnly(false);
        ui->DominanteEdit->setReadOnly(false);
        ui->Ajout_sort->setDisabled(false);
        ui->Supp_sort->setDisabled(false);
    }
}

void WidgetPersonnage::on_pushButton_clicked()
{
    MainWindow * win = qobject_cast<MainWindow*>(QApplication::activeWindow());
    win->detail_widget_personnage = nullptr;
    layout()->removeWidget(this);
    disconnect();
    setParent(nullptr);
    deleteLater();
}

void WidgetPersonnage::on_pushButton_2_clicked()
{
    Personnage *new_p = new Personnage(ui->Nom_edit->text(),ui->Niveau_edit->value(),ui->Race_edit->text(),
                                       ui->Description_edit->toPlainText(),static_cast<Sortileges*>(ui->Sortileges_edit->model())->get_sorts(),
                                       NiveauClasse(ui->Niveau_edit->value(),Classe(ui->ClasseEdit->text(),ui->DominanteEdit->text(),ui->BabEdit->value())),
                                       Caracteristique(ui->Force_edit->value(),ui->Dexterite_edit->value(),ui->Constitution_edit->value(),ui->Intelligence_edit->value(),ui->Sagesse_edit->value(),ui->Charisme_edit->value()));
    emit(edition_termine_p(new_p,current_index));
}

void WidgetPersonnage::Personnage_was_deleted(QString nom) {
    if(ui->Nom_edit->text() == nom) {
        MainWindow * win = qobject_cast<MainWindow*>(QApplication::activeWindow());
        win->detail_widget_personnage = nullptr;
        layout()->removeWidget(this);
        disconnect();
        setParent(nullptr);
        deleteLater();
    }
}

void WidgetPersonnage::on_Ajout_sort_clicked()
{
    MainWindow *win = qobject_cast<MainWindow*>(QApplication::activeWindow());
    Sortileges *sorts = win->modelSort;
    Classe *c = new Classe(ui->ClasseEdit->text(),ui->DominanteEdit->text(),ui->BabEdit->value());
    NiveauClasse *nc = new NiveauClasse(ui->Niveau_edit->value(),*c);
    WidgetAjoutSorts *w = new WidgetAjoutSorts(*sorts,*nc);
    w->show();
}

void WidgetPersonnage::Sort_recu(QString nom){
    Sortilege s(nom, QString(""),0,false,QString(""),universelle,QList<Composante>(),QList<NiveauClasse>());
    modelSort->add_item(s);
    ui->Sortileges_edit->setModel(modelSort);
}

void WidgetPersonnage::on_Supp_sort_clicked()
{
    modelSort->remove_item(sort_index);
    ui->Sortileges_edit->setModel(modelSort);
}

void WidgetPersonnage::on_Sortileges_edit_clicked(const QModelIndex &index)
{
    sort_index = index.row();
}
