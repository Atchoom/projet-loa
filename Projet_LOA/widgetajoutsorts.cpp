#include "widgetajoutsorts.h"
#include "ui_widgetajoutsorts.h"


WidgetAjoutSorts::WidgetAjoutSorts(Sortileges &sorts, NiveauClasse &c, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::WidgetAjoutSorts),
    current_index(0)
{
    ui->setupUi(this);
    QList<QString> *l = sorts.filter_list_by_class(c);
    QStringList list = QStringList(*l);
    QStringListModel *model = new QStringListModel(list);
    ui->listView->setModel(model);

    MainWindow *win = qobject_cast<MainWindow*>(QApplication::activeWindow());
    connect(this,&WidgetAjoutSorts::sort_selected,win->detail_widget_personnage,&WidgetPersonnage::Sort_recu);
}

WidgetAjoutSorts::~WidgetAjoutSorts()
{
    delete ui;
}

void WidgetAjoutSorts::on_listView_clicked(const QModelIndex &index)
{
    current_index = index.row();
}

void WidgetAjoutSorts::on_Ajouter_clicked()
{
    QString res = qvariant_cast<QString>(ui->listView->model()->data(ui->listView->model()->index(current_index,0)));
    emit(sort_selected(res));
    this->deleteLater();
}

void WidgetAjoutSorts::on_Annuler_clicked()
{
    this->deleteLater();
}
