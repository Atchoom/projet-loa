#ifndef NIVEAUCLASSE_H
#define NIVEAUCLASSE_H
#include "classe.h"

/**
 * \class NiveauClasse
 * \brief Niveau d'un personnage ou d'un sort dans une classe donnée.
 */
class NiveauClasse
{
private:
    int niveau; /*!< Le niveau de l'élément dans la classe */
    Classe classe; /*!< La classe correspondante */
public:

    /**
      * \brief Constructeur par défaut
      */
    NiveauClasse():
        niveau(0),
        classe(Classe())
    {}

    /**
     * \brief Constructeur valué
     */
    NiveauClasse(int n,const Classe & c);

    //Getters
    int get_niveau() const{
        return niveau;
    }
    Classe get_classe() const{
        return classe;
    }

    //Setters
    void set_niveau(const int n){
        niveau = n;
    }
    void set_classe(const Classe & c){
        classe = c;
    }

    /**
     * \brief Génère une chaîne de caractères
     * \details Écrit tous les attributs séparés par des ";"
     * \return Une chaîne de caractères au format csv
     */
    QString toString() const;
};

#endif // NIVEAUCLASSE_H
