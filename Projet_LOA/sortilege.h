#ifndef SORTILEGE_H
#define SORTILEGE_H
#include <QString>
#include "niveauclasse.h"
#include <QList>

/**
  * \enum
  * \brief Décrit les différentes écoles de magie.
  */
typedef enum
{
    abjuration,
    divination,
    enchantement,
    evocation,
    illusion,
    invocation,
    necromancie,
    transmutation,
    universelle
} Ecole;

/**
  * \enum
  * \brief Décrit les composantes nécessaires à l'incantation d'un sort.
  */
typedef enum
{
    gestuelle,
    verbale,
    materielle,
    focaliseur,
    focaliseur_divin
} Composante;

/**
 * \class Sortilege
 * \brief Décrit un sortilège de jeu de rôle.
 * \details Décrit et manipule les informations d'un sortilège de jeu de rôle.
 */
class Sortilege
{
private:
    QString nom; /*!< Le nom du sortilège */
    QString tps_inc; /*!< Le temps d'incantation */
    int portee; /*!< La portée du sort */
    bool resist; /*!< Le sort est-il contrable */
    QString desc; /*!< Une description du sort */
    Ecole ecole; /*!< L'école de magie du sort */
    QList<Composante> composantes; /*!< Les composantes du sort */
    QList<NiveauClasse> niveau; /*!< Les classes attribuées au sort */

public:
    //construct
    /**
     * \brief Constructeur par défaut
     */
    Sortilege(){}

    /**
     * \brief Constructeur valué
     */
    Sortilege(const QString & n, const QString & t, int p, bool r, const QString & d,const Ecole & e,const QList<Composante> & c, const QList<NiveauClasse> & lvl);

    //getters
    QString get_nom() const {
        return nom;
    }
    QString get_tps_inc() const {
        return tps_inc;
    }
    int get_portee() const {
        return portee;
    }
    bool get_resist() const {
        return resist;
    }
    QString get_desc() const {
        return desc;
    }
    Ecole get_ecole() const {
        return ecole;
    }
    QList<Composante> get_composantes() const {
        return composantes;
    }
    QList<NiveauClasse> get_niveau() const {
        return niveau;
    }

    //setters
    void set_nom(const QString & n) {
        nom = n;
    }
    void set_tps_inc(const QString & t) {
        tps_inc = t;
    }
    void set_portee(int p) {
        portee = p;
    }
    void set_resist(bool r) {
        resist = r;
    }
    void set_desc(const QString & d) {
        desc = d;
    }
    void set_ecole(Ecole & e)  {
        ecole = e;
    }
    void set_composantes(QList<Composante> & c) {
        composantes = c;
    }
    void set_niveau(QList<NiveauClasse> & n) {
        niveau = n;
    }

    /**
     * \brief Génère une chaîne de caractères
     * \details Décrit entièrement le sortilège, ses classes et ses composantes récursivement au format csv.
     * \return Une chaîne de caractères au format csv
     */
    QString toString() const;

    bool Check_sort_classe_compatibilite(NiveauClasse &c);

    bool operator==(Sortilege s2) {
        return (nom == s2.get_nom());
    }
};

#endif // SORTILEGE_H
