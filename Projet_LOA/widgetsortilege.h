#ifndef WIDGETSORTILEGE_H
#define WIDGETSORTILEGE_H

#include <QWidget>
#include "sortilege.h"
#include "niveauclasses.h"

namespace Ui {
/**
 * \class WidgetSortilege
 * \brief Widget pour les détails d'un sortilège
 */
class WidgetSortilege;
}

class WidgetSortilege : public QWidget
{
    Q_OBJECT
public:
    /**
     * \brief Constructeur par défaut
     */
    explicit WidgetSortilege(QWidget *parent = 0);

    /**
     * \brief Constructeur valué
     */
    WidgetSortilege(QWidget *parent,Sortilege &s, int i);

    /**
     * \brief Change le personnage affiché dans le widget
     * \details Change tous les champs du widget sortilège.
     * \param p Le nouveau sortilège à afficher
     * \param i L'index du sortilège dans le modèle
     */
    void ChangeSortilege(Sortilege &p, int i);
    ~WidgetSortilege();
    int current_index;  /*!< L'index du sort qui est selectionné */

private slots:
    /**
     * \brief Ferme le widget personnage
     */
    void on_pushButton_clicked();
    /**
     * \brief Permet d'éditer le personnage
     */
    void on_pushButton_3_clicked();
    /**
     * \brief Sauvegarde l'édition
     */
    void on_pushButton_2_clicked();

    void Sort_was_deleted(QString nom);

    void on_Ajout_classe_clicked();

    void on_Supp_classe_clicked();

    void on_Classes_edit_clicked(const QModelIndex &index);

public slots:
    void classe_choisie(NiveauClasse c);

private:
    Ui::WidgetSortilege *ui;  /*!< L'ui */
    bool Edition;  /*!< Pour savoir si on est en mode édition */
    NiveauClasses *modelClasses;  /*!< Le modèle des classes */
    int classe_index;  /*!< L'index de la classe */

signals:
    void edition_termine_s(Sortilege *s, int i);
};

#endif // WIDGETSORTILEGE_H
