#ifndef WIDGETAJOUTCLASSE_H
#define WIDGETAJOUTCLASSE_H

#include <QDialog>
#include "niveauclasse.h"
#include "mainwindow.h"

namespace Ui {
class widgetajoutclasse;
}

class widgetajoutclasse : public QDialog
{
    Q_OBJECT

public:
    explicit widgetajoutclasse(QWidget *parent = 0);
    ~widgetajoutclasse();

private slots:
    void on_pushButton_clicked();

    void on_pushButton_2_clicked();

private:
    Ui::widgetajoutclasse *ui;

signals:
    void classe_selected(NiveauClasse nc);
};

#endif // WIDGETAJOUTCLASSE_H
