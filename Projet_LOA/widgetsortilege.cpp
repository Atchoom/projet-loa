#include "widgetsortilege.h"
#include "ui_widgetsortilege.h"
#include "mainwindow.h"
#include "sortilege.h"
#include "widgetajoutclasse.h"

WidgetSortilege::WidgetSortilege(QWidget *parent) :
    QWidget(parent),
    current_index(0),
    ui(new Ui::WidgetSortilege),
    Edition(false),
    modelClasses(nullptr),
    classe_index(0)

{
    ui->setupUi(this);
}

void SetReadOnly(QWidget* widget, bool readOnly)
{
   widget->setAttribute(Qt::WA_TransparentForMouseEvents, readOnly);
   widget->setFocusPolicy(readOnly ? Qt::NoFocus : Qt::StrongFocus);
}

WidgetSortilege::WidgetSortilege(QWidget *parent, Sortilege &s, int i) :
    QWidget(parent),
    ui(new Ui::WidgetSortilege),
    Edition(false),
    classe_index(0)
{
    current_index = i;

    QRegExpValidator* validator = new QRegExpValidator(QRegExp("[^;]*"),this);

    ui->setupUi(this);
    ui->Nom_edit->setReadOnly(true);
    ui->Nom_edit->setText(s.get_nom());
    ui->Nom_edit->setValidator(validator);
    SetReadOnly(ui->Ecole_edit,true);
    ui->Ecole_edit->setCurrentIndex(s.get_ecole());
    QList<Composante> comp= s.get_composantes();
    ui->BoxGestuelle->setChecked(comp.contains(gestuelle));
    ui->BoxVerbale->setChecked(comp.contains(verbale));
    ui->BoxFocalisateur->setChecked(comp.contains(focaliseur));
    ui->BoxFocalisateurDivin->setChecked(comp.contains(focaliseur_divin));
    ui->BoxMaterielle->setChecked(comp.contains(materielle));
    SetReadOnly(ui->BoxVerbale,true);
    SetReadOnly(ui->BoxFocalisateur,true);
    SetReadOnly(ui->BoxFocalisateurDivin,true);
    SetReadOnly(ui->BoxGestuelle,true);
    SetReadOnly(ui->BoxMaterielle,true);
    ui->Resistance_edit->setChecked(s.get_resist());
    SetReadOnly(ui->Resistance_edit,true);
    ui->Portee_edit->setValue(s.get_portee());
    ui->Portee_edit->setReadOnly(true);
    ui->Temps_edit->setText(s.get_tps_inc());
    ui->Temps_edit->setReadOnly(true);
    ui->Temps_edit->setValidator(validator);
    ui->Description_edit->setText(s.get_desc());
    ui->Description_edit->setReadOnly(true);

    modelClasses = new NiveauClasses(s.get_niveau(),ui->Classes_edit);
    ui->Classes_edit->setModel(modelClasses);


    MainWindow * win = qobject_cast<MainWindow*>(QApplication::activeWindow());
    connect(win,&MainWindow::Sort_deleted,this,&WidgetSortilege::Sort_was_deleted);
}

void WidgetSortilege::ChangeSortilege(Sortilege &s, int i)
{
    current_index = i;

    ui->Nom_edit->setReadOnly(true);
    ui->Nom_edit->setText(s.get_nom());
    SetReadOnly(ui->Ecole_edit,true);
    ui->Ecole_edit->setCurrentIndex(s.get_ecole());
    QList<Composante> comp= s.get_composantes();
    ui->BoxGestuelle->setChecked(comp.contains(gestuelle));
    ui->BoxVerbale->setChecked(comp.contains(verbale));
    ui->BoxFocalisateur->setChecked(comp.contains(focaliseur));
    ui->BoxFocalisateurDivin->setChecked(comp.contains(focaliseur_divin));
    ui->BoxMaterielle->setChecked(comp.contains(materielle));
    SetReadOnly(ui->BoxVerbale,true);
    SetReadOnly(ui->BoxFocalisateur,true);
    SetReadOnly(ui->BoxFocalisateurDivin,true);
    SetReadOnly(ui->BoxGestuelle,true);
    SetReadOnly(ui->BoxMaterielle,true);
    ui->Resistance_edit->setChecked(s.get_resist());
    SetReadOnly(ui->Resistance_edit,true);
    ui->Portee_edit->setValue(s.get_portee());
    ui->Portee_edit->setReadOnly(true);
    ui->Temps_edit->setText(s.get_tps_inc());
    ui->Temps_edit->setReadOnly(true);
    ui->Description_edit->setText(s.get_desc());
    ui->Description_edit->setReadOnly(true);

    modelClasses = new NiveauClasses(s.get_niveau(),ui->Classes_edit);
    ui->Classes_edit->setModel(modelClasses);


}

WidgetSortilege::~WidgetSortilege()
{
    delete ui;
}

void WidgetSortilege::on_pushButton_clicked()
{
    MainWindow * win = qobject_cast<MainWindow*>(QApplication::activeWindow());
    win->detail_widget_sortilege = nullptr;
    layout()->removeWidget(this);
    disconnect();
    setParent(nullptr);
    deleteLater();
}

void WidgetSortilege::on_pushButton_3_clicked()
{
    if(!Edition) {
        ui->Nom_edit->setReadOnly(false);
        ui->Ecole_edit->setEnabled(true);
        SetReadOnly(ui->Ecole_edit,false);
        SetReadOnly(ui->BoxVerbale,false);
        SetReadOnly(ui->BoxFocalisateur,false);
        SetReadOnly(ui->BoxFocalisateurDivin,false);
        SetReadOnly(ui->BoxGestuelle,false);
        SetReadOnly(ui->BoxMaterielle,false);
        SetReadOnly(ui->Resistance_edit,false);
        ui->Portee_edit->setReadOnly(false);
        ui->Temps_edit->setReadOnly(false);
        ui->Description_edit->setReadOnly(false);
    }
}

void WidgetSortilege::on_pushButton_2_clicked()
{
    QList<Composante> comp;
    if(ui->BoxGestuelle->isChecked())
        comp.append(gestuelle);
    if(ui->BoxVerbale->isChecked())
        comp.append(verbale);
    if(ui->BoxMaterielle->isChecked())
        comp.append(materielle);
    if(ui->BoxFocalisateurDivin->isChecked())
        comp.append(focaliseur_divin);
    if(ui->BoxFocalisateur->isChecked())
        comp.append(focaliseur);
    Ecole ecole = Ecole(ui->Ecole_edit->currentIndex());
    Sortilege *new_s = new Sortilege(ui->Nom_edit->text(),ui->Temps_edit->text(),ui->Portee_edit->value(),ui->Resistance_edit->isChecked(),ui->Description_edit->toPlainText(),
                                     ecole,comp,static_cast<NiveauClasses*>(ui->Classes_edit->model())->get_classes());
    emit(edition_termine_s(new_s,current_index));
}

void WidgetSortilege::Sort_was_deleted(QString nom){
    if(ui->Nom_edit->text() == nom) {
        MainWindow * win = qobject_cast<MainWindow*>(QApplication::activeWindow());
        win->detail_widget_sortilege = nullptr;
        layout()->removeWidget(this);
        disconnect();
        setParent(nullptr);
        deleteLater();
    }
}

void WidgetSortilege::on_Ajout_classe_clicked()
{
    widgetajoutclasse *wg = new widgetajoutclasse();
    wg->show();
}

void WidgetSortilege::classe_choisie(NiveauClasse c){
    modelClasses->add_item(c);
    ui->Classes_edit->setModel(modelClasses);
}

void WidgetSortilege::on_Supp_classe_clicked()
{
    modelClasses->remove_item(classe_index);
    ui->Classes_edit->setModel(modelClasses);
}

void WidgetSortilege::on_Classes_edit_clicked(const QModelIndex &index)
{
    classe_index = index.row();
}
