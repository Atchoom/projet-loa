#ifndef CARACTERISTIQUE_H
#define CARACTERISTIQUE_H

/**
 * \class Caracteristique
 * \brief Gère les caractéristiques d'un personnage.
 * \date jeudi 28 mars
 */
class Caracteristique
{
private:
    int force; /*!< La valeur de force du personnage */
    int dexterite; /*!< La valeur de dexterite du personnage */
    int constitution; /*!< La valeur de constitution du personnage */
    int intelligence; /*!< La valeur de intelligence du personnage */
    int sagesse; /*!< La valeur de sagesse du personnage */
    int charisme; /*!< La valeur de charisme du personnage */

public:

    /**
     * \brief Constructeur par défaut
     */
    Caracteristique();

    /**
     * \brief Constructeur valué
     */
    Caracteristique(int str, int dex, int cons, int inte, int sag, int cha);


    //Getters de la classe
    int get_force() const{
        return force;
    }
    int get_dexterite() const{
        return dexterite;
    }
    int get_constitution() const{
        return constitution;
    }
    int get_intelligence() const{
        return intelligence;
    }
    int get_sagesse() const{
        return sagesse;
    }
    int get_charisme() const{
        return charisme;
    }

    //Setters de la classe
    void set_force(int value){
        force = value;
    }
    void set_dexterite(int value){
        dexterite = value;
    }
    void set_constitution(int value){
        constitution = value;
    }
    void set_intelligence(int value){
        intelligence = value;
    }
    void set_sagesse(int value){
        sagesse = value;
    }
    void set_charisme(int value){
        charisme = value;
    }
};

#endif // CARACTERISTIQUE_H
