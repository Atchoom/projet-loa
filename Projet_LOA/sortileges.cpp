#include "sortileges.h"

int Sortileges::rowCount(const QModelIndex &parent) const{
    parent.flags();
    return this->sorts.count();
}

QVariant Sortileges::data(const QModelIndex &index, int role) const{
    if (! index.isValid())
        return QVariant();
    if(index.row()>=this->sorts.count())
        return QVariant();
    if (role == Qt::DisplayRole)
        return this->sorts.at(index.row()).get_nom();
    if (role == Qt::EditRole)
        return QVariant::fromValue<Sortilege>(this->sorts.at(index.row()));
    return QVariant();
}

QList<Sortilege> Sortileges::get_sorts(){
    return sorts;
}

bool Sortileges::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if (index.isValid() && role == Qt::EditRole)
    {
        this->sorts.replace(index.row(), qvariant_cast<Sortilege>(value));
        emit(dataChanged(index, index));
        return true;
    }
    return false;
}

void Sortileges::remove_item(int index){
    sorts.removeAt(index);
}

void Sortileges::add_item(Sortilege s){
    sorts.append(s);
}

QList<QString> *Sortileges::filter_list_by_class(NiveauClasse &c){
    QList<QString> *result = new QList<QString>();
    foreach (Sortilege s, sorts) {
        if(s.Check_sort_classe_compatibilite(c)){
            result->append(s.get_nom());
        }
    }
    return result;
}



