#include "mainwindow.h"
#include "ui_mainwindow.h"



MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    detail_widget_personnage(nullptr),
    detail_widget_sortilege(nullptr),
    modelSort(new Sortileges(QList<Sortilege>())),
    modelPerso(new Personnages(QList<Personnage>())),
    ui(new Ui::MainWindow),
    file(""),
    current_index(QModelIndex())
{
    ui->setupUi(this);

    Classe *c1 = new Classe("Oracle de la vie","Charisme",0.75);
    Classe *c2 = new Classe("Barbare rageux","Force",1);
    NiveauClasse *nc1 = new NiveauClasse(20,*c1);
    NiveauClasse *nc2 = new NiveauClasse(1,*c2);

    QList<Composante> lc;
    lc << gestuelle;
    lc << verbale;

    QList<NiveauClasse> lnc;
    lnc << *nc1;
    lnc << *nc2;

    Sortilege *s1 = new Sortilege("Avadakedabra","inf",20,false,"La mort",universelle,lc,lnc);
    Sortilege *s2 = new Sortilege("Leviosaaaa","inf",40,true,"Stahp",evocation,QList<Composante>(),lnc);
    QList<Sortilege> sorts;
    sorts << *s1 << *s2;
    modelSort = new Sortileges(sorts,ui->listViewSort);

    QList<Sortilege> *ls1 = new QList<Sortilege>();
    ls1->append(*s1);
    QList<Sortilege> *ls2 = new QList<Sortilege>();
    ls2->append(*s2);
    ls2->append(*s1);
    Caracteristique c(10,10,10,15,15,15);
    Personnage *p1 = new Personnage("JeanEudes",5,"Horloge","Il aime bien le chocolat",*ls1,*nc1,c);
    Personnage *p2 = new Personnage("JeanJacques",45654,"Cactus/Humain/Pot","Il aime bien le chocolat chaud",*ls2,*nc1,c);
    QList<Personnage> persos;
    persos << *p2 << *p1 << *p1;
    modelPerso = new Personnages(persos,ui->listViewPerso);

    //Sortileges *modelSort = new Sortileges(QList<Sortilege>(),ui->listViewSort);
    //Personnages *modelPerso = new Personnages(QList<Personnage>(),ui->listViewPerso);

    QSortFilterProxyModel *proxyPerso = new QSortFilterProxyModel;
    proxyPerso->setSourceModel(modelPerso);
    proxyPerso->sort(0);
    ui->listViewPerso->setModel(proxyPerso);

    QSortFilterProxyModel *proxySort = new QSortFilterProxyModel;
    proxySort->setSourceModel(modelSort);
    proxySort->sort(0);
    ui->listViewSort->setModel(proxySort);


    //ui->listViewPerso->setModel(modelPerso);
    //ui->listViewSort->setModel(modelSorts);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_actionEnregistrer_triggered()
{
    if (file.isEmpty()){
        on_actionEnregistrer_sous_triggered();
    }
    else{
        saveFile(file);
    }
}

void MainWindow::on_actionEnregistrer_sous_triggered()
{
    QString fileName = QFileDialog::getSaveFileName(
                this, tr("Enregister sous..."), "", tr("Fichiers texte (*.txt)"));
    if (!fileName.isEmpty())
    {
        saveFile(fileName);
        file = fileName;
    }
}

void MainWindow::saveFile(const QString & fileName)
{
    QFile file(fileName);
    if (file.open(QIODevice::WriteOnly))
    {
        QTextStream ts(&file);
        ts << QString::number(ui->listViewPerso->model()->rowCount())+QString("\n");
        for (int i = 0;i<ui->listViewPerso->model()->rowCount();i++){
                ts << qvariant_cast<Personnage>(ui->listViewPerso->model()->data(ui->listViewPerso->model()->index(i,0),Qt::EditRole)).toString().toStdString().c_str()+QString("\n");
        }

        ts << QString::number(ui->listViewSort->model()->rowCount())+QString("\n");
        for (int i = 0;i<ui->listViewSort->model()->rowCount();i++){
                ts << qvariant_cast<Sortilege>(ui->listViewSort->model()->data(ui->listViewSort->model()->index(i,0),Qt::EditRole)).toString()+QString("\n");
        }

        ts.flush();
        file.close();
    }
    else
        QMessageBox::critical(
            this,
            tr("Echec de sauvegarde"),
            tr("Le fichier ") + fileName + tr(" n'a pas pu être sauvegarde."));
}

void MainWindow::on_listViewPerso_doubleClicked(const QModelIndex &index)
{
    current_index = index;
    Personnage selected = qvariant_cast<Personnage>(ui->listViewPerso->model()->data(index,Qt::EditRole));
    if(detail_widget_personnage){
        detail_widget_personnage->ChangePersonnage(selected,index.row());
    }
    else {
        detail_widget_personnage = new WidgetPersonnage(this,selected,index.row());
        connect(detail_widget_personnage,&WidgetPersonnage::sortilege_choisi,this,&MainWindow::sortilege_recu);
        connect(detail_widget_personnage,&WidgetPersonnage::edition_termine_p,this,&MainWindow::edition_personnage_recu);
        ui->horizontalLayout_3->addWidget(detail_widget_personnage);
    }
}


void MainWindow::on_listViewSort_doubleClicked(const QModelIndex &index)
{
    Sortilege selected = qvariant_cast<Sortilege>(ui->listViewSort->model()->data(index,Qt::EditRole));
    if(detail_widget_sortilege)
    {
        detail_widget_sortilege->ChangeSortilege(selected,index.row());
    }
    else {
        detail_widget_sortilege = new WidgetSortilege(this,selected,index.row());
        connect(detail_widget_sortilege,&WidgetSortilege::edition_termine_s,this,&MainWindow::edition_sortilege_recu);
        ui->horizontalLayout_3->addWidget(detail_widget_sortilege);
    }
}

void MainWindow::sortilege_recu(Sortilege *s, int i)
{
    if(detail_widget_sortilege)
    {
       detail_widget_sortilege->ChangeSortilege(*s,i);
    }
    else {
        detail_widget_sortilege = new WidgetSortilege(this,*s,i);
        connect(detail_widget_sortilege,&WidgetSortilege::edition_termine_s,this,&MainWindow::edition_sortilege_recu);
        ui->horizontalLayout_3->addWidget(detail_widget_sortilege);
    }
}

void MainWindow::edition_personnage_recu(Personnage *p,int i)
{
    ui->listViewPerso->model()->setData(ui->listViewPerso->model()->index(i,0),QVariant::fromValue<Personnage>(*p),Qt::EditRole);
}

void MainWindow::edition_sortilege_recu(Sortilege *s,int i)
{
    ui->listViewSort->model()->setData(ui->listViewSort->model()->index(i,0),QVariant::fromValue<Sortilege>(*s),Qt::EditRole);
}

void MainWindow::on_actionOuvrir_triggered()
{
    QString fileName = QFileDialog::getOpenFileName(this,
                                                    tr("Ouvrir un fichier"),
                                                    "",
                                                    tr("Text files (*.txt)"));
    if (!fileName.isEmpty())
    {
        readFile(fileName);
        file = fileName;
    }
}

void MainWindow::readFile(const QString & fileName)
{
    QFile file(fileName);
    if (file.open(QIODevice::ReadOnly))
    {
        //Lit les personnages
        QTextStream ts(&file);
        QList<Personnage> listPerso;
        int numberP = ts.read(1).toInt(); //nombre de personnage à lire
        ts.readLine();
        for (int Nperso=0;Nperso<numberP;Nperso++)
        {
            QString select = ts.readLine();
            QStringList perso = select.split(';');
            int index = 0; //index pour lire un personnage (une ligne) transformé en QStringList

            QString nom = perso.at(index++);//0
            int niveau = perso.at(index++).toInt();//1
            QString race = perso.at(index++);//2
            QString desc = perso.at(index++);//3
            int nbSort = (perso.at(index++)).toInt();//4
            QList<Sortilege> sorts;
            int indexSort = 0;
            //Lit l'ensemble des sorts du personnage
            while (indexSort<nbSort){

                QString nom_sort = perso.at(index++);//0
                QString tps_sort = perso.at(index++);//1
                int portee_sort = perso.at(index++).toInt();//2
                bool resist = perso.at(index++).toInt() == 1;//3
                QString desc = perso.at(index++);//4
                Ecole ecole;
                switch(perso.at(index++).toInt()){
                   case 0:
                    ecole = abjuration;
                    break;
                   case 1:
                    ecole = divination;
                    break;
                   case 2:
                    ecole = enchantement;
                    break;
                   case 3:
                    ecole = evocation;
                    break;
                   case 4:
                    ecole = illusion;
                    break;
                   case 5:
                    ecole = invocation;
                    break;
                   case 6:
                    ecole = necromancie;
                    break;
                   case 7:
                    ecole = transmutation;
                    break;
                   case 8:
                    ecole = universelle;
                    break;
                   default:
                    break;
                }//5

                int nbComp = perso.at(index).toInt()+index;
                index++;
                //Lit les composantes d'un sort du personnage
                QList<Composante> comp;
                while (index<nbComp+1){
                    switch(perso.at(index++).toInt()){
                       case 0:
                        comp.append(gestuelle);
                        break;
                       case 1:
                        comp.append(verbale);
                        break;
                       case 2:
                        comp.append(materielle);
                        break;
                       case 3:
                        comp.append(focaliseur);
                        break;
                       case 4:
                        comp.append(focaliseur_divin);
                       default:
                        break;
                    }
                }

                int nbClasse = perso.at(index).toInt()*4+index;
                index++;
                QList<NiveauClasse> classes;
                Classe *classe;
                while (index<nbClasse){
                    int niv = perso.at(index++).toInt();
                    classe = new Classe(perso.at(index++),perso.at(index++),perso.at(index++).toFloat());
                    classes.append(NiveauClasse(niv,*classe));
                }

                Sortilege* nouveau = new Sortilege(nom_sort,tps_sort,portee_sort,resist,desc,ecole,comp,classes);
                sorts.append(*nouveau);
                indexSort++;
            }
            NiveauClasse *classe = new NiveauClasse(perso.at(index).toInt(),Classe(perso.at(index),perso.at(index),perso.at(index).toFloat()));
            index = index+1;

            Caracteristique carac;
            carac.set_force(perso.at(index++).toInt());
            carac.set_dexterite(perso.at(index++).toInt());
            carac.set_constitution(perso.at(index++).toInt());
            carac.set_intelligence(perso.at(index++).toInt());
            carac.set_sagesse(perso.at(index++).toInt());
            carac.set_charisme(perso.at(index++).toInt());

            //Ajoute le personnage à la liste de personnage
            Personnage* nouveau = new Personnage(nom,niveau,race,desc,sorts,*classe,carac);
            listPerso.append(*nouveau);
        }


        //Lire les sorts
        int numberS = ts.read(1).toInt();
        QList<Sortilege> listSorts;
        ts.readLine();
        for(int Nsort=0;Nsort<numberS;Nsort++)
        {
            QString select = ts.readLine();
            QStringList sort = select.split(';');
            int index = 0;

            QString nom = sort.at(index++);//0
            QString tps = sort.at(index++);//1
            int portee = sort.at(index++).toInt();//2
            bool resist = sort.at(index++).toInt() == 1;//3
            QString desc = sort.at(index++);//4
            Ecole ecole;
            switch(sort.at(index++).toInt()){
               case 0:
                ecole = abjuration;
                break;
               case 1:
                ecole = divination;
                break;
               case 2:
                ecole = enchantement;
                break;
               case 3:
                ecole = evocation;
                break;
               case 4:
                ecole = illusion;
                break;
               case 5:
                ecole = invocation;
                break;
               case 6:
                ecole = necromancie;
                break;
               case 7:
                ecole = transmutation;
                break;
               case 8:
                ecole = universelle;
                break;
               default:
                break;
            }//5

            int nbComp = sort.at(index).toInt()+index;
            index++;
            QList<Composante> comp;
            while (index<nbComp+1){
                switch(sort.at(index++).toInt()){
                   case 0:
                    comp.append(gestuelle);
                    break;
                   case 1:
                    comp.append(verbale);
                    break;
                   case 2:
                    comp.append(materielle);
                    break;
                   case 3:
                    comp.append(focaliseur);
                    break;
                   case 4:
                    comp.append(focaliseur_divin);
                   default:
                    break;
                }
            }

            int nbClasse = sort.at(index).toInt()*4+index;
            index++;
            QList<NiveauClasse> classes;
            Classe *classe;
            while (index<nbClasse){
                int niv = sort.at(index++).toInt();
                classe = new Classe(sort.at(index++),sort.at(index++),sort.at(index++).toFloat());
                classes.append(NiveauClasse(niv,*classe));
            }

            Sortilege* nouveau = new Sortilege(nom,tps,portee,resist,desc,ecole,comp,classes);
            listSorts.append(*nouveau);
        }

        modelPerso = new Personnages(listPerso,ui->listViewPerso);
        modelSort = new Sortileges(listSorts,ui->listViewSort);

        QSortFilterProxyModel *proxyPerso = new QSortFilterProxyModel;
        proxyPerso->setSourceModel(modelPerso);
        proxyPerso->sort(0);
        ui->listViewPerso->setModel(proxyPerso);

        QSortFilterProxyModel *proxySort = new QSortFilterProxyModel;
        proxySort->setSourceModel(modelSort);
        proxySort->sort(0);
        ui->listViewSort->setModel(proxySort);

        //ui->listViewPerso->setModel(modelPerso);
        //ui->listViewSort->setModel(modelSort);

        file.close();
    }
}


void MainWindow::on_pushButtonPerso_clicked()
{
    QString pattern = ui->lineEditPerso->text();

    QSortFilterProxyModel *proxyPerso = new QSortFilterProxyModel;
    proxyPerso->setSourceModel(modelPerso);
    proxyPerso->setFilterRegExp(pattern);
    proxyPerso->sort(0);
    ui->listViewPerso->setModel(proxyPerso);
}

void MainWindow::on_pushButtonSort_clicked()
{
    QString pattern = ui->lineEditSort->text();

    QSortFilterProxyModel *proxySort = new QSortFilterProxyModel;
    proxySort->setSourceModel(modelSort);
    proxySort->setFilterRegExp(pattern);
    proxySort->sort(0);
    ui->listViewSort->setModel(proxySort);
}

void MainWindow::on_Supprimer_clicked()
{
    if(current_index.isValid()) {
        QString name = qvariant_cast<QString>(modelPerso->data(current_index,Qt::DisplayRole));
        modelPerso->remove_item(current_index.row());
        QSortFilterProxyModel *proxyPerso = new QSortFilterProxyModel;
        proxyPerso->setSourceModel(modelPerso);
        proxyPerso->sort(0);
        ui->listViewPerso->setModel(proxyPerso);
        emit(Personnage_deleted(name));
    }
}

void MainWindow::on_Nouveau_clicked()
{
    Caracteristique c(0,0,0,0,0,0);
    Personnage p(QString("Nouveau Personnage"),0,QString(""),QString(""),QList<Sortilege>(),NiveauClasse(),c);
    modelPerso->add_item(p);
    QSortFilterProxyModel *proxyPerso = new QSortFilterProxyModel;
    proxyPerso->setSourceModel(modelPerso);
    proxyPerso->sort(0);
    ui->listViewPerso->setModel(proxyPerso);
}

void MainWindow::on_listViewPerso_clicked(const QModelIndex &index)
{
    current_index = index;
}

void MainWindow::on_Nouveau_2_clicked()
{
    Sortilege s(QString("Nouveau Sort"), QString(""),0,false,QString(""),universelle,QList<Composante>(),QList<NiveauClasse>());
    modelSort->add_item(s);
    QSortFilterProxyModel *proxySort = new QSortFilterProxyModel;
    proxySort->setSourceModel(modelSort);
    proxySort->sort(0);
    ui->listViewSort->setModel(proxySort);
}

void MainWindow::on_Supprimer_2_clicked()
{
    if(current_index.isValid()) {
        QString name = qvariant_cast<QString>(modelSort->data(current_index,Qt::DisplayRole));
        modelSort->remove_item(current_index.row());
        QSortFilterProxyModel *proxySort = new QSortFilterProxyModel;
        proxySort->setSourceModel(modelSort);
        proxySort->sort(0);
        ui->listViewSort->setModel(proxySort);
        emit(Sort_deleted(name));
    }
}

void MainWindow::on_listViewSort_clicked(const QModelIndex &index)
{
    current_index = index;
}

void MainWindow::on_actionAbout_triggered()
{
    About *a = new About(this);
    a->show();
}
