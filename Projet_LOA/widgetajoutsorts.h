#ifndef WIDGETAJOUTSORTS_H
#define WIDGETAJOUTSORTS_H

#include <QDialog>
#include "sortileges.h"
#include "niveauclasse.h"
#include <QStringListModel>
#include "mainwindow.h"

namespace Ui {
/**
 * \class WidgetAjoutSorts
 * \brief Gère la fenêtre pour ajouter un sort au personnage.
 * \date jeudi 28 mars
 */
class WidgetAjoutSorts;
}

class WidgetAjoutSorts : public QDialog
{
    Q_OBJECT

public:
    /**
     * \brief Constructeur valué
     */
    explicit WidgetAjoutSorts(Sortileges &sorts, NiveauClasse &c,QWidget *parent = 0);
    ~WidgetAjoutSorts();

private slots:

    /**
     * \brief Enregistre l'index de l'élément ciblé
     * \param index L'élément ciblé
     */
    void on_listView_clicked(const QModelIndex &index);

    /**
     * \brief Ajoute le sort au modèle
     */
    void on_Ajouter_clicked();

    /**
     * \brief Annule le sort créé
     */
    void on_Annuler_clicked();

private:
    Ui::WidgetAjoutSorts *ui; /*!< L'interface graphique */
    int current_index; /*!< Index de l'élément ciblé */

signals:
    void sort_selected(QString nom);
};

#endif // WIDGETAJOUTSORTS_H
