#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QFileDialog>
#include <QMessageBox>
#include "widgetsortilege.h"
#include <QSortFilterProxyModel>
#include "personnages.h"
#include "sortileges.h"
#include "about.h"
#include "widgetpersonnage.h"
#include <QRegExp>
#include <QValidator>
#include <iostream>

namespace Ui {

/**
 * \class mainwindow.h
 * \brief Gère la fenêtre principale et l'interface graphique.
 * \date jeudi 28 mars
 */
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    WidgetPersonnage *detail_widget_personnage; /*!< Le widget de description d'un personnage */
    WidgetSortilege *detail_widget_sortilege;/*!< Le widget de description d'un sort */
    Sortileges *modelSort;/*!< Modèle de sorts courant */
    Personnages *modelPerso;/*!< Modèle de personnages courant */

    /**
     * \brief Constructeur par défaut
     */
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    /**
     * \brief Getter de l'attribut Ui
     */
    Ui::MainWindow *getUi() {
        return ui;
    }

private slots:

    /**
     * \brief Sélection d'un personnage dans la liste
     * \details Crée ou remplace le widget Personnage avec les caractéristiques de l'élément ciblé.
     * \param index L'index du personnage sélectionné
     */
    void on_listViewPerso_doubleClicked(const QModelIndex &index);

    /**
     * \brief Sélection d'un sort dans la liste
     * \details Crée ou remplace le widget Sort avec les caractéristiques de l'élément ciblé.
     * \param index L'index du sort sélectionné
     */
    void on_listViewSort_doubleClicked(const QModelIndex &index);

    /**
     * \brief Enregistrement rapide
     * \details Enregistre le fichier au format csv pour être réimporté via l'action ouvrir.
     */
    void on_actionEnregistrer_triggered();

    /**
     * \brief Enregistrement
     * \details Ouvre une boîte de dialogue pour enregistrer les modèles au format csv, affecte également un nom au fichier.
     */
    void on_actionEnregistrer_sous_triggered();

    /**
     * \brief Crée le widget sortilège avec le sort passé en paramètre
     * \param s Le sortilège à décrire
     */
    void sortilege_recu(Sortilege *s, int i);

    /**
     * \brief Permet d'éditer un sortilège via son widget de description
     * \param s Le sortilège à décrire
     * \param i L'index de ce sort dans le modèle
     */
    void edition_sortilege_recu(Sortilege *s, int i);

    /**
     * \brief Permet d'éditer un personnage via son widget de description
     * \param s Le personnaeg à décrire
     * \param i L'index de ce personnage dans le modèle
     */
    void edition_personnage_recu(Personnage *p, int i);

    /**
     * \brief Ouverture d'un fichier
     * \details Ouvre une boîte de dialogue changer le modèle pour contenir les données d'un fichier csv.
     */
    void on_actionOuvrir_triggered();

    /**
     * \brief Recherche de nom de personnage
     * \details Change le modèle pour qu'il ne contienne que les éléments triés dont le nom match avec la barre de recherche de personnage.
     */
    void on_pushButtonPerso_clicked();

    /**
     * \brief Recherche de nom de sort
     * \details Change le modèle pour qu'il ne contienne que les éléments triés dont le nom match avec la barre de recherche de sort.
     */
    void on_pushButtonSort_clicked();

    /**
     * \brief Supprimer l'élément sélectionné
     * \details Supprime l'élément sélectionné du modèle
     */
    void on_Supprimer_clicked();

    /**
     * \brief Crée un nouvel élément avec les valeurs par défaut
     */
    void on_Nouveau_clicked();

    /**
     * \brief Enregistre l'index de l'élement sélectionné
     * \param index L'index de l'élément
     */
    void on_listViewPerso_clicked(const QModelIndex &index);

    /**
     * \brief Crée un nouvel élément avec les valeurs par défaut
     */
    void on_Nouveau_2_clicked();

    /**
     * \brief Supprimer l'élément sélectionné
     * \details Supprime l'élément sélectionné du modèle
     */
    void on_Supprimer_2_clicked();

    /**
     * \brief Enregistre l'index de l'élement sélectionné
     * \param index L'index de l'élément
     */
    void on_listViewSort_clicked(const QModelIndex &index);

    /**
     * \brief Génère la bulle About
     */
    void on_actionAbout_triggered();

private:
    Ui::MainWindow *ui;/*!< Interface graphique*/
    QString file;/*!< Nom du fichier courant */
    QModelIndex current_index; /*!< Index de l'élément sélectionné */


    /**
     * \brief Sauvegarder les modèles courants dans un fichier
     * \details Écris les personnages et les sorts dans le fichier nommé "filename" au format csv.
     * \param filename L'addresse où enregistrer les modèles
     * \attention Le chemin doit être accessible en écriture.
     */
    void saveFile(const QString & filename);

    /**
     * \brief Modifie les modèles courants depuis un fichier
     * \details Lis les personnages et les sorts depuis le fichier nommé "filename" via un format csv et remplace les modèles courants.
     * \param filename L'addresse où enregistrer les modèles
     * \attention Le chemin doit être accessible en lecture et au format csv.
     * \attention Toutes les chaînes de caractères doivent ne pas contenir le symbole ";".
     */
    void readFile(const QString & filename);

signals:
    /**
     * \brief Sort supprimé
     */
    void Sort_deleted(QString nom);

    /**
     * \brief Personnage supprimé
     */
    void Personnage_deleted(QString nom);
};

#endif // MAINWINDOW_H
