#ifndef PERSONNAGES_H
#define PERSONNAGES_H
#include <QAbstractListModel>
#include <QModelIndex>
#include <QList>
#include "personnage.h"

Q_DECLARE_METATYPE(Personnage)

/**
 * \class Personnages
 * \brief Modèle de personnage.
 * \details Cette classe hérite d'un modèle de liste et permet d'afficher les personnages dans l'interface graphique et de les éditer.
 */
class Personnages : public QAbstractListModel
{
private:
    QList<Personnage> personnageListe; /*!< Liste des personnages à afficher */
public:  

    /**
     * \brief Constructeur valué
     * \param pl Liste des personnages à afficher
     */
    Personnages(const QList<Personnage> &pl, QObject *parent = 0):QAbstractListModel(parent) {
        if (pl.count() > 0)
            this->personnageListe = pl;
    }

    /**
     * \brief Renvoie le nombre d'éléments de la liste
     * \param parent Le widget parent du modèle.
     * \return Le nombre de ligne du modèle
     */
    int rowCount(const QModelIndex &parent) const;

    /**
     * \brief Accéder un élément du modèle
     * \param index L'index de l'élément à afficher
     * \param role Le rôle de l'élement (Display pour affichage seul, Edit pour permettre son édition)
     * \return Le nom de l'élément de rôle Display ou l'élément de rôle Edit à l'index \a index.
     */
    QVariant data(const QModelIndex &index, int role) const;

    bool setData(const QModelIndex &index, const QVariant &value, int role);

    /**
     * \brief Supprime un élément du modèle
     * \param index L'élément à supprimer
     */
    void remove_item(int index);
    /**
     * \brief Ajoute un élément au modèle
     * \param p L'élément à ajouter
     */
    void add_item(Personnage &p);


};

#endif // PERSONNAGES_H
