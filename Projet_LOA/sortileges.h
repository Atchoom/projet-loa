#ifndef SORTILEGETABLEMODEL_H
#define SORTILEGETABLEMODEL_H

#include <QAbstractListModel>
#include <QList>
#include "sortilege.h"
#include <iostream>

Q_DECLARE_METATYPE(Sortilege)

/**
 * \class Sortileges
 * \brief Modèle de sortilege.
 * \details Cette classe hérite d'un modèle de liste et permet d'afficher les sortileges dans l'interface graphique et de les éditer.
 */
class Sortileges : public QAbstractListModel
{
private:
    QList<Sortilege> sorts; /*!< La liste des sortilèges à afficher */
public:
    /**
     * \brief Constructeur valué
     * \param sorts Liste des sorts à afficher
     */
    Sortileges(const QList<Sortilege> &sorts, QObject *parent = 0):QAbstractListModel(parent)
    {
        if (sorts.count() > 0) this->sorts = sorts;
    }

    /**
     * \brief Renvoie le nombre d'éléments de la liste
     * \param parent Le widget parent du modèle.
     * \return Le nombre de ligne du modèle
     */
    int rowCount(const QModelIndex &parent = QModelIndex()) const;

    /**
     * \brief Accéder un élément du modèle
     * \param index L'index de l'élément à afficher
     * \param role Le rôle de l'élement (Display pour affichage seul, Edit pour permettre son édition)
     * \return Le nom de l'élément de rôle Display ou l'élément de rôle Edit à l'index \a index.
     */
    QVariant data(const QModelIndex &index, int role) const;

    QList<Sortilege> get_sorts();
    
    /**
     * \brief Change la valeur d'un élément du modèle
     * \param index L'index de l'élément à modifier
     * \param value La nouvelle valeur de l'élément
     * \param role Display ou Edit
     * \return
     */
    bool setData(const QModelIndex &index, const QVariant &value, int role);

    /**
     * \brief Supprime un élément du modèle
     * \param index L'élément à supprimer
     */
    void remove_item(int index);
    
    /**
     * \brief Ajoute un élément au modèle
     * \param p L'élément à ajouter
     */
    void add_item(Sortilege s);

    /**
     * \brief Filtrer le modèle par classe
     * \param c La classe voulue
     * \return  La liste des noms des sorts contenant la classe voulue ou d'un niveau inférieur.
     */
    QList<QString> *filter_list_by_class(NiveauClasse &c);

};

#endif // SORTILEGETABLEMODEL_H
