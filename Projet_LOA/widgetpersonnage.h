#ifndef WIDGETPERSONNAGE_H
#define WIDGETPERSONNAGE_H

#include <QWidget>
#include "personnage.h"
#include "sortileges.h"


namespace Ui {
/**
 * \class WidgetPersonnage
 * \brief Widget pour les détails d'un personnage
 */
class WidgetPersonnage;
}

class WidgetPersonnage : public QWidget
{
    Q_OBJECT
public:
    /**
     * \brief Constructeur par défaut
     */
    explicit WidgetPersonnage(QWidget *parent = 0);

    /**
     * \brief Constructeur valué
     */
    WidgetPersonnage(QWidget *parent,Personnage &p, int i);

    /**
     * \brief Change le personnage affiché dans le widget
     * \details Change tous les champs du widget personnage.
     * \param p Le nouveau personnage à afficher
     * \param i L'index du personnage dans le modele
     */
    void ChangePersonnage(Personnage &p, int i);
    ~WidgetPersonnage();
    int current_index;
    int sort_index;

private slots:

    /**
     * \brief Affiche le sort dans le widget sortilège
     * \param index L'index du sort à afficher
     */
    void on_Sortileges_edit_doubleClicked(const QModelIndex &index);

    /**
     * \brief Passe en mode Édition
     */
    void on_pushButton_3_clicked();

    /**
     * \brief Ferme le widget
     */
    void on_pushButton_clicked();

    /**
     * \brief Sauvegarde le personnage en cours
     */
    void on_pushButton_2_clicked();

    /**
     * \brief Ferme la fenêtre à la suppression du personnage
     * \param nom
     */
    void Personnage_was_deleted(QString nom);

    /**
     * \brief Ajoute un personnage
     */
    void on_Ajout_sort_clicked();

    void on_Supp_sort_clicked();

    void on_Sortileges_edit_clicked(const QModelIndex &index);

public slots:
    void Sort_recu(QString nom);

private:
    Ui::WidgetPersonnage *ui; /*!< Interface graphique */
    bool Edition; /*!< mode édition */
    Sortileges  *modelSort; /*!< le modèle de sort */

signals:
    void sortilege_choisi(Sortilege *s, int i);
    void edition_termine_p(Personnage *p, int i);
};

#endif // WIDGETPERSONNAGE_H
