#include "caracteristique.h"

Caracteristique::Caracteristique(){

}

Caracteristique::Caracteristique(int str, int dex, int cons, int inte, int sag, int cha) :
    force(str),
    dexterite(dex),
    constitution(cons),
    intelligence(inte),
    sagesse(sag),
    charisme(cha)
{
}
