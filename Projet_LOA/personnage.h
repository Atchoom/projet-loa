#ifndef PERSONNAGE_H
#define PERSONNAGE_H
#include <QString>
#include "sortilege.h"
#include "niveauclasse.h"
#include "caracteristique.h"
#include <iostream>
#include <QTextStream>

using namespace std;

/**
 * \class Personnage
 * \brief Décrit un personnage de jeu de rôle.
 * \details Décrit et manipule les informations d'un personnage de jeu de rôle.
 */
class Personnage
{
private:
    QString nom; /*!< Le nom du personnage */
    int niveau; /*!< Le niveau général du personnage */
    QString race; /*!< La race du personnage */
    QString desc; /*!< Une description du personnage */
    QList<Sortilege> sorts; /*!< La liste des sorts appris par le personnage */
    NiveauClasse classes; /*!< La liste des classes du personnage */
    Caracteristique carac; /*!< Les caractéristiques de personnage */
public:
    /**
     * \brief Constructeur par défaut
     */
    Personnage();

    /**
     * \brief Constructeur valué
     */
    Personnage(const QString & n, int lvl, const QString & r, const QString & d, const QList<Sortilege> & s, const NiveauClasse&  c, const Caracteristique cr);

    //getters
    QString get_nom() const{
        return nom;
    }
    int get_niveau() const{
        return niveau;
    }
    QString get_race() const{
        return race;
    }
    QString get_desc() const{
        return desc;
    }
    QList<Sortilege> get_sorts() const{
        return sorts;
    }
    NiveauClasse get_classes() const{
        return classes;
    }
    Caracteristique get_carac() const{
        return carac;
    }

    //setters
    void set_name(const QString & n){
        nom = n;
    }
    void set_niveau(int n){
        niveau = n;
    }
    void set_race(const QString & r){
        race = r;
    }
    void set_desc(const QString & d){
        desc = d;
    }
    void set_sorts(const QList<Sortilege> & s){
        sorts = s;
    }
    void set_classes(const NiveauClasse & c){
        classes = c;
    }
    void set_carac(const Caracteristique & c){
        carac = c;
    }

    /**
     * \brief Génère une chaîne de caractères
     * \details Décrit entièrement le personnage, ses classes et ses sortilèges récursivement au format csv.
     * \return Une chaîne de caractères au format csv
     */
    QString toString() const;
};


#endif // PERSONNAGE_H
