#ifndef NIVEAUCLASSES_H
#define NIVEAUCLASSES_H
#include <QAbstractListModel>
#include <QModelIndex>
#include <QList>
#include "niveauclasse.h"

Q_DECLARE_METATYPE(NiveauClasse)

/**
 * \class NiveauClasses
 * \brief Modèle de NiveauClasse.
 * \details Cette classe hérite d'un modèle de liste et permet d'afficher les NiveauClasses dans l'interface graphique et de les éditer.
 */
class NiveauClasses : public QAbstractListModel
{
private:
    QList<NiveauClasse> classeListe; /*!< Liste des classes à afficher */
public:

    /**
     * \brief Constructeur valué
     * \param pl Liste des classes à afficher
     */
    NiveauClasses(const QList<NiveauClasse> &pl, QObject *parent = 0):QAbstractListModel(parent) {
        if (pl.count() > 0)
            this->classeListe = pl;
    }

    /**
     * \brief Renvoie le nombre d'éléments de la liste
     * \param parent Le widget parent du modèle.
     * \return Le nombre de ligne du modèle
     */
    int rowCount(const QModelIndex &parent) const;

    /**
     * \brief Accéder un élément du modèle
     * \param index L'index de l'élément à afficher
     * \param role Le rôle de l'élement (Display pour affichage seul, Edit pour permettre son édition)
     * \return Le nom de l'élément de rôle Display ou l'élément de rôle Edit à l'index \a index.
     */
    QVariant data(const QModelIndex &index, int role) const;

    bool setData(const QModelIndex &index, const QVariant &value, int role);

    QList<NiveauClasse> get_classes();

    void remove_item(int index);

    void add_item(NiveauClasse s);
};

#endif // NIVEAUCLASSES_H
