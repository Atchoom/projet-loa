#include "niveauclasses.h"

int NiveauClasses::rowCount(const QModelIndex &parent) const{
    parent.flags();
    return this->classeListe.count();
}

QVariant NiveauClasses::data(const QModelIndex &index, int role) const{
    if (! index.isValid())
        return QVariant();
    if(index.row()>=this->classeListe.count())
        return QVariant();
    if (role == Qt::DisplayRole){
        return (classeListe.at(index.row()).get_classe().get_nom())+(" de niveau ")+QString::number(classeListe.at(index.row()).get_niveau());
}
    if (role == Qt::EditRole){
        return QVariant::fromValue<NiveauClasse>(this->classeListe.at(index.row()));
    }
    return QVariant();
}

bool NiveauClasses::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if (index.isValid() && role == Qt::EditRole)
    {
        this->classeListe.replace(index.row(), qvariant_cast<NiveauClasse>(value));
        emit(dataChanged(index, index));
        return true;
    }
    return false;
}

QList<NiveauClasse> NiveauClasses::get_classes(){
    return classeListe;
}

void NiveauClasses::remove_item(int index){
    classeListe.removeAt(index);
}

void NiveauClasses::add_item(NiveauClasse s){
    classeListe.append(s);
}
