#include "classe.h"

Classe::Classe(const QString & n,const QString & d,float b) :
    nom(n),
    dominante(d),
    bab(b)
{
}

QString Classe::toString() const
{
    QString res;
    res = get_nom()+";"+get_dominante()+";"+QString::number(get_bab());
    return res;
}
