#include "widgetajoutclasse.h"
#include "ui_widgetajoutclasse.h"


widgetajoutclasse::widgetajoutclasse(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::widgetajoutclasse)
{
    ui->setupUi(this);
    MainWindow *win = qobject_cast<MainWindow*>(QApplication::activeWindow());
    connect(this,&widgetajoutclasse::classe_selected,win->detail_widget_sortilege,&WidgetSortilege::classe_choisie);
}

widgetajoutclasse::~widgetajoutclasse()
{
    delete ui;
}

void widgetajoutclasse::on_pushButton_clicked()
{
    this->deleteLater();
}

void widgetajoutclasse::on_pushButton_2_clicked()
{
    Classe *c = new Classe(ui->nom->text(),ui->carac->text(),float(ui->bab->value())/100);
    NiveauClasse *nc = new NiveauClasse(ui->niveau->value(),*c);
    emit(classe_selected(*nc));
    this->deleteLater();
}
