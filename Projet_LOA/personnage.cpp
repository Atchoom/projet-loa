#include "personnage.h"

Personnage::Personnage(){

}

Personnage::Personnage(const QString & n, int lvl, const QString & r, const QString & d, const QList<Sortilege> & s, const NiveauClasse&  c, const Caracteristique cr):
    nom(n),niveau(lvl),race(r),desc(d),sorts(s),classes(c),carac(cr)
{
}

QString Personnage::toString() const
{
    QString res = get_nom()+";"+QString::number(get_niveau())+";"+get_race()+";"+get_desc()+";"+QString::number(get_sorts().count())+";";
    for (int i=0;i<get_sorts().count();i++){
        res = res + (get_sorts().at(i)).toString();
    }
    res = res + (get_classes()).toString()+";";
    res = res + QString::number(get_carac().get_force())+";" + QString::number(get_carac().get_dexterite())+";" + QString::number(get_carac().get_constitution())+";" + QString::number(get_carac().get_intelligence())+";" + QString::number(get_carac().get_sagesse())+";" + QString::number(get_carac().get_charisme())+";";
    return res;
}
