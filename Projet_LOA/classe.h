#ifndef CLASSE_H
#define CLASSE_H
#include <QString>

/**
 * \class Classe
 * \brief Décrit les différentes classes de personnages.
 */
class Classe
{
private:
    QString nom; /*!< Le nom de la classe */
    QString dominante; /*!< La caractéristique dominante de la classe, celle qui permet de calculer les dégâts */
    float bab; /*!< Capacité inné de la classe à se battre (vaut 0.5,0.75 ou 1) */
public:

    /**
      * \brief Constructeur par défaut
      */
    Classe() :
        nom("SansClasse"),
        dominante(""),
        bab(0)
    {}

    /**
     * \brief Constructeur valué
     */
    Classe(const QString & nom,const QString & dominante, float bab);

    //Getters de la classe
    QString get_nom() const{
        return nom;
    }
    QString get_dominante() const{
        return dominante;
    }
    float get_bab() const {
        return bab;
    }

    //Setters de la classe
    void set_nom(const QString & n){
        nom = n;
    }
    void set_dominante(const QString & d){
        dominante = d;
    }
    void set_bab( float b){
        bab = b;
    }

    /**
     * \brief Génère une chaîne de caractères
     * \details Écrit tous les attributs de la classe séparés par des ";"
     * \return Une chaîne de caractères au format csv
     */
    QString toString() const;
};

#endif // CLASSE_H
