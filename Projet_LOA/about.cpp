#include "about.h"
#include "ui_about.h"

About::About(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::About)
{
    ui->setupUi(this);

    setFixedSize(313,138);  //fixe la taille de la fenêtre
}

About::~About()
{
    delete ui;
}

void About::on_pushButton_clicked()
{
    this->setParent(nullptr);
    this->deleteLater();
}
