#include "sortilege.h"

Sortilege::Sortilege(const QString & n, const QString & t, int p, bool r, const QString & d,const Ecole & e,const QList<Composante> & c, const QList<NiveauClasse> & lvl) :
    nom(n),tps_inc(t), portee(p), resist(r),  desc(d), ecole(e), composantes(c), niveau(lvl)
{
}

QString Sortilege::toString() const
{
    QString res = get_nom()+";"+get_tps_inc() +";"+QString::number(get_portee())+";"+QString::number(get_resist())+";"+get_desc()+";"+QString::number(get_ecole())+";";
    res = res + QString::number(get_composantes().count())+";";
    for (int i=0;i<get_composantes().count();i++){
        res = res + QString::number(get_composantes().at(i)) + ";";
    }
    res = res + QString::number(get_niveau().count())+";";
    for (int i=0;i<get_niveau().count();i++){
        res = res + get_niveau().at(i).toString() + ";";
    }
    return res;
}

bool Sortilege::Check_sort_classe_compatibilite(NiveauClasse &c){
    QList<NiveauClasse> classes = niveau;
    foreach (NiveauClasse classe, classes) {
        if((classe.get_classe().get_nom()==c.get_classe().get_nom()) && (classe.get_niveau() >= c.get_niveau()))
            return true;
    }
    return false;
}
