#-------------------------------------------------
#
# Project created by QtCreator 2019-03-06T09:25:04
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Projet_LOA
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += \
    main.cpp \
    mainwindow.cpp \
    sortilege.cpp \
    classe.cpp \
    niveauclasse.cpp \
    personnage.cpp \
    caracteristique.cpp \
    personnages.cpp \
    sortileges.cpp \
    widgetpersonnage.cpp \
    widgetsortilege.cpp \
    niveauclasses.cpp \
    widgetajoutsorts.cpp \
    about.cpp \
    widgetajoutclasse.cpp

HEADERS += \
    mainwindow.h \
    sortilege.h \
    classe.h \
    niveauclasse.h \
    personnage.h \
    caracteristique.h \
    personnages.h \
    sortileges.h \
    widgetpersonnage.h \
    widgetsortilege.h \
    about.h \
    niveauclasses.h \
    widgetajoutsorts.h \ 
    widgetajoutclasse.h

FORMS += \
        mainwindow.ui \
    widgetpersonnage.ui \
    widgetsortilege.ui \
    widgetajoutsorts.ui \
    about.ui \
    widgetajoutclasse.ui

RESOURCES += \
    logo.qrc
