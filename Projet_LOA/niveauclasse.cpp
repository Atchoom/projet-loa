#include "niveauclasse.h"

NiveauClasse::NiveauClasse(int n,const Classe & c) :
    niveau(n),
    classe(c)
{
}

QString NiveauClasse::toString() const
{
    return QString::number(get_niveau())+";"+get_classe().toString();
}
