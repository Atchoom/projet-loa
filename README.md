# Projet de Langage Objet Avancé

Projet de 2ème année à l'ENSIIE, dans le cadre du cours de Langage Objet Avancé.
Il s'agit d'une application de gestion d'éléments, réalisée sous Qt.
Elle a été réalisé par Thibault Lextrait et Timothée Denoux.
Elle permet la gestion et la création de fiches Personnage et de sortilèges pour le jeu de rôle Pathfinder.