#include "personnages.h"

int Personnages::rowCount(const QModelIndex &parent) const{
    parent.flags();
    return this->personnageListe.count();
}

QVariant Personnages::data(const QModelIndex &index, int role) const{
    if (! index.isValid())
        return QVariant();
    if(index.row()>=this->personnageListe.count())
        return QVariant();
    if (role == Qt::DisplayRole){
        return this->personnageListe.at(index.row()).get_nom();
}
    if (role == Qt::EditRole){
        return QVariant::fromValue<Personnage>(this->personnageListe.at(index.row()));
    }
    return QVariant();
}

bool Personnages::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if (index.isValid() && role == Qt::EditRole)
    {
        this->personnageListe.replace(index.row(), qvariant_cast<Personnage>(value));
        emit(dataChanged(index, index));
        return true;
    }
    return false;
}

void Personnages::remove_item(int index){
    personnageListe.removeAt(index);
}

void Personnages::add_item(Personnage &p){
    personnageListe.append(p);
}
